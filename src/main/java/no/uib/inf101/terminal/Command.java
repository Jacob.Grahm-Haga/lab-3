package no.uib.inf101.terminal;

public interface Command {
    String getName();
    String run(String[] args);
}
