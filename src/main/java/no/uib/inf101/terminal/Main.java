package no.uib.inf101.terminal;

// UiB INF101 ShellLab - Main.java
// Dette er filen som inneholder main-metoden.

public class Main {

  public static void main(String[] args) {
    SimpleShell shell = new SimpleShell();
    shell.installCommand(new CmdEcho());
    
    Terminal gui = new Terminal(shell);
    gui.run();
    
  }
}